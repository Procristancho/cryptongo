import requests # esta libreria me permite hacer consultas a una url especifica
import pymongo

API_URL = 'https://api.coinmarketcap.com/v1/ticker/'

#conexion a la base de datos mongo
def get_db_connection(uri):
    client = pymongo.MongoClient(uri)
    return client.cryptongo  # este cliente es como el shell de mongo

# obtenemos los datos de la url
def get_cryptocurrencies_from_api():
    r = requests.get(API_URL)
    if r.status_code == 200:
        result = r.json()
        return result

    raise Exception('API Error')

# la funcion first_element permite ordenar la informacion
def first_element(elements):
    return elements[0]

# la funcion get_hash convierte el strig de get_ticker_hash y lo encripta usando sha512
def get_hash(value):
    from hashlib import sha512
    return sha512(value.encode('utf-8')).hexdigest()


# la funcion get_ticker_hash convierte el ticker en un string largo
def get_ticker_hash(ticker_data):
    from collections import OrderedDict
    ticker_data = OrderedDict(
        sorted(
            ticker_data.items(),
            key = first_element
        )
    )

    ticker_value = ''
    for _, value in ticker_data.items():
        ticker_value += str(value)

    return get_hash(ticker_value)

# Sorted sirve para ordenar estructuras de datos, puede ser de menor a mayor, etc
# items trae todos los elementos de la variable ticker_data


def check_if_exists(db_connection, ticker_data):
    ticker_hash = get_ticker_hash(ticker_data)
    if db_connection.tickers.find_one({'ticker_hash': ticker_hash}):
        return True

    return False


def save_ticker(db_connection, ticker_data=None):
    if not ticker_data:
        return False # si no recibe nada el ticker_data retornamos False

    if check_if_exists(db_connection, ticker_data):
        return False # si la informacion ya existe retornamos False

    ticker_hash = get_ticker_hash(ticker_data)
    ticker_data['ticker_hash'] = ticker_hash
    ticker_data['rank'] = int(ticker_data['rank']) # convierto el rank que me llega como string en un entero
    ticker_data['last_updated'] = int(ticker_data['last_updated']) # convierto el last_updated que me llega como string en un entero

    db_connection.tickers.insert_one(ticker_data)
    return True


if __name__ == "__main__":
    connection = get_db_connection('mongodb://localhost:27017/')
    tickers = get_cryptocurrencies_from_api()

    for ticker in tickers:
        save_ticker(connection, ticker)


    print("Tickers almacenados exitosamente")
