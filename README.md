# Estructura del proyecto
Este proyecto esta dividido en dos pequeñas partes la carpeta **agente** y la carpeta 
**api**; en la carpeta agente estará el archivo con las funciones que necesitaremos para
obtener la información desde la API de **coinmarketcap**, y enviarla a la base de datos MongoDB.

En la carpeta **API** estarán la funciones necesarias para mostrar la información.

## librerias 
Se debe instalar de manera previa la libreria pymongo utilizando el comando `pip install pymongo`.

